---
features:
  top:
  - name: "GitLab Duo Chat now generally available"
    available_in: [premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/gitlab_duo_chat.html'
    video: 'https://www.youtube-nocookie.com/embed/ZQBAuf-CTAY'
    reporter: tlinz
    stage: ai-powered
    stage_url: 'https://about.gitlab.com/gitlab-duo/'
    categories:
    - Duo Chat
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13516'
    description: |
      GitLab Duo Chat is now [generally available](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#generally-available-ga). As part of this release, we are also making these capabilities generally available:

      - Code explanation helps developers and less technical users understand unfamiliar code faster
      - Code refactoring enables developers to simplify and improve existing code
      - Test generation automates repetitive tasks and helps teams catch bugs sooner

      Users can access GitLab Duo Chat in the GitLab UI, in the Web IDE, in VS Code, or in JetBrains IDEs.

      Learn more about this release of GitLab Duo Chat from this [blog post](https://about.gitlab.com/blog/2024/04/18/gitlab-duo-chat-now-generally-available/).

      Chat is currently freely accessible by all Ultimate and Premium users. Instance administrators, group owners, and project owners can choose to [restrict Duo features from accessing and processing their data](https://docs.gitlab.com/ee/user/ai_features_enable.html#turn-off-gitlab-duo-features).

      The GitLab Duo Chat is part of [GitLab Duo Pro](https://about.gitlab.com/gitlab-duo/#pricing). To ease the transition for Chat beta users who have yet to purchase GitLab Duo Pro, Duo Chat will remain available to existing Premium and Ultimate customers (without the add-on) for a short period of time. We will announce when access will be restricted to Duo Pro subscribers at a later date.

      Feel free to share your thoughts by clicking the feedback button in the chat or by creating an issue and mentioning GitLab Duo Chat. We’d love to hear from you!
