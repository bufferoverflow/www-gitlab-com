---
layout: markdown_page
title: "Category Direction - Notifications"
---

- TOC
{:toc}

|                       |                               |
| -                     | -                             |
| **Stage** | [Foundations](/direction/foundations/) |
| **Maturity** | [Minimal](https://handbook.gitlab.com/handbook/product/ux/category-maturity/category-maturity-scorecards/) |
| **Last reviewed** | `2024-05-10` |

## Introduction and how you can help

<%= partial("../templates/intro.html", { locals: { context: "Notifications category", label: "Category:Notifications" }}) %>

## Strategy and themes

People using GitLab can guide work throughout the entire DevSecOps lifecycle &mdash; from planning, to development, verification, and ultimately delivery. Having all of these capabilities within a single tool is powerful and efficient, yet requires users to stay up-to-date on changes and discussion happening across many parts of the product.

GitLab to-dos are the product experience intended to address this need. However they were designed to adhere to the GTD model: the only "notifications" we surfaced within GitLab were actionable tasks for the user. Our research has found that deciding what's actionable is a very personal, subjective question that varies by person &mdash; what some users find actionable, others find as noise, and vice versa. Instead, users want more visibility into their notifications so _they_ can decide which they need to act on.

In contrast, email notifications provide an exhaustive set of notifications for nearly every event that happens in GitLab. Some GitLab users have abandoned to-dos entirely in favor of email notifications, as this gives a more thorough way to stay up-to-date. While this gives them more visibility into events occuring in their projects, they must spend significant time each day processing their email inbox to avoid missing an update. Additionally, they are required to jump between email and GitLab as they triage and take action on notifications.

We want users to stay up-to-date within GitLab more efficiently than they could via any other tool. We will do this by:

* Surfacing the notifications they need directly within GitLab
* Creating an integrated experience between notifications and the actions they prompt
* Providing capabilities that allow them to focus on relevant notifications

## 1 year plan

[**Introduce GitLab notifications center**](https://gitlab.com/groups/gitlab-org/-/epics/13794)

Users need a simpler, more efficient way to stay up-to-date on work happening in GitLab. We will add a native notifications center to the platform, and allow people to triage, manage and action notifications within the product. We are currently designing this experience and will add more details once we have validated these designs.

[**Keep discussions in GitLab**](https://gitlab.com/groups/gitlab-org/-/epics/13580)

Team leads want to be notified of all comments in a discussion so they can take action even when they're not explicitly mentioned. Currently, GitLab only creates todos to review a comment when you are directly mentioned. This drives team leads rely on email notifications, which leaves them bouncing back and forth between GitLab and their email inbox.

GitLab users should be able to keep up with conversations within GitLab. We will introduce additional web notifications for replies to discussions you've participated in, and improve the behavior for automatically resolving notifications to work better with discussion threads.

[**Provide fine-grained control for web notifications**](https://gitlab.com/groups/gitlab-org/-/epics/13796)

Introducing more notifications into GitLab risks creating a new problem: making noise that distracts users from focusing on creating software. The types of notifications that users care about vary from person to person, so we need to provide tooling that allows users to tailor their notifications to exactly what they need to remain productive.

[**Quickly review a large volume of notifications**](https://gitlab.com/groups/gitlab-org/-/epics/13797)

Even with support for customizing notifications, people can still end up with a large inbox of notifications to review. We want to provide ways for users to quickly review a large volume of notifications. We are considering ideas like:

* Summarizing notifications with GitLab Duo
* More hueristics for automatically clearing irrelevant notifications
* Email digests

### What we recently completed

**Problem validation**

We have audited previous research, user comments, and internal explorations to understand the core challenges and opportunities in this space. We are now conducting light-weight user research to validate the key themes we uncovered before moving forward to address them.

### What is next for us

See our [roadmap in GitLab](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=group::personal%20productivity&label_name%5B%5D=direction&label_name%5B%5D=Category:Notifications&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false).

### What we are currently working on

See the [current Personal productivity milestone plan](/direction/manage/personal_productivity#what-are-we-currently-working-on) on our group direction page.

## Target audience

Notifications are used by all users, so our long-term vision must accomodate a variety of needs. In order to make progress, we will focus on a subset of personas that have a pressing need to remain up-to-date with work happening in GitLab:

* [Parker (Product Manager)](https://handbook.gitlab.com/handbook/product/personas/#parker-product-manager)
* [Delaney (Development Team Lead)](https://handbook.gitlab.com/handbook/product/personas/#delaney-development-team-lead)

Once we have created an experience that they love, we will broaden our focus to other people that use GitLab daily. We see the following personas as being our key targets in this second phase:

* [Presley (Product Designer)](https://handbook.gitlab.com/handbook/product/personas/#presley-product-designer)
* [Sasha (Software Developer)](https://handbook.gitlab.com/handbook/product/personas/#sasha-software-developer)
* [Priyanka (Platform Engineer)](https://handbook.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
* [Rachel (Release Manager)](https://handbook.gitlab.com/handbook/product/personas/#rachel-release-manager)
* [Simone (Software Engineer in Test)](https://handbook.gitlab.com/handbook/product/personas/#simone-software-engineer-in-test)
* [Amy (Application Security Engineer)](https://handbook.gitlab.com/handbook/product/personas/#amy-application-security-engineer)
* [Isaac (Infrastructure Engineer)](https://handbook.gitlab.com/handbook/product/personas/#isaac-infrastructure-security-engineer)
* [Alex (Security Operations Engineer)](https://handbook.gitlab.com/handbook/product/personas/#alex-security-operations-engineer)

## Best in class landscape

While Notifications doesn't represent a competitive category, the core user experience of GitLab will impact how well GitLab performs in the market. We take inspiration from others in the field that are delivering exceptional notification experiences.

**GitHub**

GitHub introduced their current notifications experience in [2020](https://github.blog/2020-04-22-improving-notifications-for-everyone/). They received significant praise for this upon launch, and we continue to hear requests from customers to bring an equivalent experience to GitLab. There are a few key details that we've noted as being particularly impactful:

* Allowing users to view notifications in the web or their email inbox, based on their preference
* Providing granular notification settings all the way down to the issue level
* Offering a slick mobile interface to support on-the-go conversations

**Linear**

The [founder and original designer](https://www.linkedin.com/in/karrisaarinen/) of Linear had years of experience as a designer before starting Linear, so it's no surprise their product is beautifully and thoughtfully designed. The [inbox view](https://linear.app/docs/inbox) is a core part of the Linear workflow, so it is a mature feature packed with innovative details:

* Master / detail view for efficient notification triage
* Keyboard actions to further speed up triage
* Snoozing notifications to follow up on them later

**GitDock**

Built by GitLab Team Member [Marcel van Remmerden](https://gitlab.com/mvanremmerden), [GitDock](https://gitlab.com/mvanremmerden/gitdock) is a MacOS/Windows/Linux app that displays all your GitLab activities in one place. Instead of the GitLab typical project- or group-centric approach, it collects all your information from a user-centric perspective.

**GitLight**

GitLight is an [open-source tool](https://github.com/colinlienard/gitlight) for managing notifications from GitHub and GitLab in a single interface. Like GitDock, this tool is building on top of the concepts offered by GitLab to create new experiences. It has some advanced concepts that make it stand out:

* Kanban-like interface for notifications
* Grouping of notifications by issue
* Sophisticated system for prioritizing notifications based on user-defined rules

**Honorable mentions**

Notification experiences are not unique to developer tools &mdash; they're a common feature in collaboration software. We're looking beyond tools in our space to see how other domains are solving this problem as well.

* Slack & various Slack apps
* Almanac.io
* HEY.com