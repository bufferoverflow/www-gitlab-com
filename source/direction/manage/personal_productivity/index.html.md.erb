---
layout: markdown_page
title: "Group Direction - Personal productivity"
description: "Overall direction for the Personal productivity group"
canonical_path: "/direction/manage/personal_productivity/"
epic_roadmap: https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=group::personal%20productivity&label_name%5B%5D=direction&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false
---

- TOC
{:toc}

| | |
| --- | --- |
| Stage | [Manage](/direction/manage) |
| Maturity | _Not scored_ |
| Content last reviewed | 2024-03-18 |

## Introduction and how you can help

Thanks for visiting the direction page for the [Personal productivity group](https://about.gitlab.com/handbook/product/categories/#personal_productivity-group) at GitLab. The Personal productivity group lives within the [Manage stage](/direction/manage) and is maintained by [Jeff Tucker](https://gitlab.com/jtucker_gl).

The Personal productivity group owns three categories within GitLab:

| Category | Direction | Description |
| --- | --- | --- |
<% for category in data.categories.values_at(*data.stages.stages.manage.groups.personal_productivity.categories) do %>
| <%= category.name %> | [Direction page](<%= category.direction %>) | <%= category.description %> |
<% end %>

If you're a GitLab user and have feedback about your needs for navigation, notifications, settings, or general product design, we would love to hear from you.

<!-- FIXME when renaming Manage stage -->
* Share feedback in a related [issue](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%personal%20productivity&first_page_size=20) or [epic](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=CREATED_AT_DESC&label_name%5B%5D=group%3A%personal%20productivity), or [on a video call](https://calendly.com/jtucker-gitlab/30min)
* To contribute, please [open an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new#) using the `~"group::personal productivity"` label
* Team members can reach us in the `#g_manage_foundations` Slack channel.

As always, this direction page is a work in progress, and everyone can contribute.

## Strategy and themes

GitLab uses the System Usability Score (SUS) to measure how effectively the GitLab product addresses user needs.
In some cases, individual teams are able to address findings from SUS surveys, such as confusion around behavior in a merge request or pipeline.
However, much of the feedback we receive is foundational to the product, discussing areas like navigation, notifications, settings, and documentation.

These cross-stage UIs and user workflows need to be consistent, clearly designed, and iterated on like a feature to improve the overall user experence of GitLab.
The primary goal of the Personal productivity team is to improve SUS scores for all of GitLab by focusing on these foundational experiences.

Some examples of foundational experiences that drive our direction:

-  Streamline navigation across the application
-  Consistent Pajamas components used every interface
-  Help users find what matters to them on the product home page
-  Help users communicate with their teams across workflows. For example, through email, todos, comments, etc.
-  Navigating to a filtered view in GitLab for a ritual such as monitoring specific security vulnerabilities, or a planning board for standup.
-  The user flow of using a new feature, navigating to the docs, and back to the feature

## 1 year plan

[**Easily switch between projects**](https://gitlab.com/groups/gitlab-org/-/epics/13076)

We conduct quarterly feedback surveys on GitLab's navigation. Users have mentioned navigating between projects as a major challenge during the most recent surveys. We will seek to improve that experience soon.

[**Search for settings via the command palette**](https://gitlab.com/groups/gitlab-org/-/epics/13078)

GitLab offers many settings across project, group, instance, and personal levels.  Users find it difficult to know where to look for a given setting. They have to spend time looking through many possible locations in order to find the right spot. We will allow users to search for settings via the command palette this year.

[**Bring a high-quality dark mode to GA**](https://gitlab.com/groups/gitlab-org/-/epics/2902)

GitLab launched an experimental version of dark mode in 2020. The approach taken in the alpha allowed us to get a usable dark mode into users' hands quickly, yet has downsides that make iteration and refinement of dark mode difficult. We are working now to create a robust dark mode design and implementation.

[**Notifications center MVC**](https://gitlab.com/groups/gitlab-org/-/epics/11609)

Many users complain that todos require excessive pruning in order to be useful. They also complain that they have to rely too much on email or third-party software in order to receive all necessary notifications. Many users mention wanting a proper notification center. We plan to release our first MVC for a GitLab notification center this year.

### What is next for us?

You can view our [roadmap in GitLab](<%= current_page.data.epic_roadmap %>).

<!--
Hoping that we can embed roadmap views one day...
<figure class="video_container">
<iframe width="560" height="315" src="<%= current_page.data.epic_roadmap %>" title="GitLab roadmap view" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</figure>
-->

### What we are currently working on

Watch our latest kickoff video to see our plans for the current milestone.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/videoseries?si=r6e5IuHE16-kvWKQ&amp;list=PL05JrBw4t0KrzdBlAcQT7_I52xFFFgBAl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</figure>

### What is not planned

The Technical Writing team are the main contributors to our documentation site. We are supporting but not actively working on improvements. In the future, we could craft workflows between GitLab and our Docs.

### Beyond 1 year

**Personalized home page**

We do not have one place in GitLab for a user to be able to easily see at a glance all relevant items they are working on and care about. The new navigation improved navigating within a project or group, but there is still not an efficient way to check on things across groups and projects, or monitor other areas of GitLab that users care about without requiring more clicks. It is also not easy to be able to check on teammates work, or to see at a high level all of the things you may need to act on. We want to create a customizable home page that allows users to tailor GitLab to their personal workflow.

**Maturing the user experience for settings**

We have consistently received feedback from customers expressing the difficulty in discovering, finding, and recalling where to find specific settings within GitLab. We want to further simplify the settings pages to aid new users in setting up their first GitLab project.

We have also heard from enterprise accounts that managing settings at the project level does not scale to meet their need of administering 1,000s of projects. We will explore approaches that allow large GitLab installations to drive consistency in how they govern their projects.

**Workflow-oriented navigation**

Customers need GitLab to offer complete solutions to complex problems. Often solving a single problem requires using many features across the product. Our long-term goal is to provide workflow-oriented navigation throughout GitLab, seamlessly connecting all of the features it offers.

## Best in Class Landscape

While Personal productivity doesn't represent a competitive group, the core user experience of GitLab will impact how well GitLab performs in the market. We take inspiration from others in the field that are delivering exceptional user experiences.

**GitHub**

GitHub is our closest competitor. Most of our users have used GitHub, and routinely compare the two products. We want to provide experiences that feel familiar to these users while still maintaining the differentiation that our complete DevSecOps platform provides.

**GitDock**

Built by GitLab Team Member [Marcel van Remmerden](https://gitlab.com/mvanremmerden), [GitDock](https://gitlab.com/mvanremmerden/gitdock) is a MacOS/Windows/Linux app that displays all your GitLab activities in one place. Instead of the GitLab typical project- or group-centric approach, it collects all your information from a user-centric perspective.

**Stripe**

[Stripe](https://stripe.com/) has long been a design-focused darling of the tech world. They have consistently delivered product experiences that center around key user workflows. We are especially inspired by how they manage site navigation with such a complex portfolio.

## Target audience

Foundational experiences are used by all users, so any changes we make must consider the needs of a broad range of personas.
That said, the areas we focus on are most impactful to people that use GitLab daily. We see the following personas as being our key targets:

* [Parker (Product Manager)](https://handbook.gitlab.com/handbook/product/personas/#parker-product-manager)
* [Delaney (Development Team Lead)](https://handbook.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
* [Presley (Product Designer)](https://handbook.gitlab.com/handbook/product/personas/#presley-product-designer)
* [Sasha (Software Developer)](https://handbook.gitlab.com/handbook/product/personas/#sasha-software-developer)
* [Priyanka (Platform Engineer)](https://handbook.gitlab.com/handbook/product/personas/#priyanka-platform-engineer)
* [Rachel (Release Manager)](https://handbook.gitlab.com/handbook/product/personas/#rachel-release-manager)
* [Simone (Software Engineer in Test)](https://handbook.gitlab.com/handbook/product/personas/#simone-software-engineer-in-test)
* [Amy (Application Security Engineer)](https://handbook.gitlab.com/handbook/product/personas/#amy-application-security-engineer)
* [Isaac (Infrastructure Engineer)](https://handbook.gitlab.com/handbook/product/personas/#isaac-infrastructure-security-engineer)
* [Alex (Security Operations Engineer)](https://handbook.gitlab.com/handbook/product/personas/#alex-security-operations-engineer)