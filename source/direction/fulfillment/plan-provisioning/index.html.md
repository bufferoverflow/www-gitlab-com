---
layout: markdown_page
title: "Category Direction - Provision - Plan Provisioning"
description: "Strategy page for the category of plan provisioning owned by group Provision."
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
 
## Mission
The Provision group's mission is to provide a seamless customer experience in accessing GitLab subscriptions, add-ons and trials, while providing key license delivery and usage data to internal teams for data-driven insights.

## Mission
The Provision group's mission is to provide a seamless customer experience in accessing GitLab subscriptions, add-ons and trials, while providing key license delivery and usage data to internal teams for data-driven insights.

## Overview
The category of `Plan Provisioning` focuses on providing access to GitLab's main subscription products for GitLab.com, self managed and GitLab Dedicated deployments. For these products, we provide purchase confirmation mailers, paid and trial feature activation/de-activation and usage data reporting.

## Team Focus Areas
### Top Priorities FY25
Over the next 12 months, the Provision team has three primary objectives:

1. Support the release of new main plan product offerings
1. Expand trial availability and experience
1. Streamline provisioning processes

#### Support release of new main plan product offerings
One of Provision's primary focus areas this year will be supporting the release of new main subscription products and features. We assist with ensuring features are accurately provisioned to customers on GitLab.com, GitLab Dedicated, and self managed instances. This yea

#### Expand and improve trial availability
In FY24, we launched the ability for [GitLab.com Premium customers to trial Ultimate functionality](https://gitlab.com/groups/gitlab-org/-/epics/9549) within their existing namespace and workflows. This year, we will continue to expand on this experience. Following that, we are planning to expand on self managed trials by doing the following: 1. [Transition Self Managed Ultimate trials to Cloud Licensing](https://gitlab.com/groups/gitlab-org/-/epics/12173)
1. [Allow Self Managed trial initiation from the customer's instance](https://gitlab.com/groups/gitlab-org/-/epics/13819)

### Streamline provisioning processes
In addition to feature projects, we are constantly iterating on our provisioning processes to make them as streamlined and efficient as possible. In FY25, we intend to work on [defining future provisioning architecture](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/7912) and create a blueprint for aligning GitLab.com, self managed and GitLab Dedicated provisioning.

Besides the top initiatives outlined in our 1 year plan, we have some additional areas of attention outlined below. For a comprehensive list of our upcoming and ongoing projects, check out our [GitLab Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Aprovision&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP).

